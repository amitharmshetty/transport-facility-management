import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransportManagementComponent } from './transport-management/transport-management.component';
import { AuthGuard, ResolverDataService } from '../utils';


const routes: Routes = [
  {
    path: '',
    component: TransportManagementComponent,
    canActivate: [AuthGuard],
    resolve: {
      rides: ResolverDataService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeTransportManagementRoutingModule { }
