import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeTransportManagementRoutingModule } from './employee-transport-management-routing.module';
import { TransportManagementComponent } from './transport-management/transport-management.component';
import { AppCommonModule } from '../app-common/app-common.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [TransportManagementComponent],
  imports: [
    CommonModule,
    EmployeeTransportManagementRoutingModule,
    AppCommonModule,
    FormsModule
  ]
})
export class EmployeeTransportManagementModule { }
