import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeForm, StoreDataService, Ride, JourneyPoint, UserBookedRide, officeLocation } from 'src/app/utils';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-transport-management',
  templateUrl: './transport-management.component.html',
  styleUrls: ['./transport-management.component.scss']
})
export class TransportManagementComponent implements OnInit {
  public empForm: EmployeeForm = new EmployeeForm();
  public availableRides: Ride[] | null = null;
  public emplyDisplayRides: Ride[] | null = null;
  public locations: JourneyPoint[] = [];
  public errorFetchingRides = false;
  public bookedRide: UserBookedRide | null = null;
  public officeLoc = officeLocation;
  public availableTimings = [];
  public bookNewRide = false;
  @ViewChild('transportForm') employeeFormRef!: NgForm;
  constructor(private storeSvc: StoreDataService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.data.pipe(
      map(data => data.rides)
    ).subscribe(
      data => {
        if (data.data) {
          this.availableRides = data.data;
        } else if (data.error) {
          this.errorFetchingRides = true;
        }
      }
    );
  }

  ngOnInit() {
    this.populateTimings();
    this.getAvailableLocations();
  }
  getAvailableLocations(): void {
    this.storeSvc.populateLocations().subscribe(
      data => {
        this.locations = data || [];
      }
    );
    this.storeSvc.getBookedRide().subscribe(
      data => {
        this.bookedRide = data;
      }
    );
  }
  private populateTimings(): void {
    let a = new Date();
    let hours = a.getHours();
    let minutes = a.getMinutes();
    if (minutes >= 30 && hours !== 23) {
      hours = hours + 1;
      minutes = 0;
    } else {
      minutes = 30;
    }
    a.setHours(hours);
    a.setMinutes(minutes);
    a.setSeconds(0);
    a.setMilliseconds(0);
    const breakpointDate = new Date();
    breakpointDate.setHours(23);
    breakpointDate.setMinutes(59);
    while (a.getTime() < breakpointDate.getTime()) {
      this.availableTimings.push(new Date(a));
      a = new Date(a.getTime() + 30 * 60 * 1000);
    }
  }
  onTravelTypeChange(travelType: string): void {
    if (this.empForm.travelType !== travelType) {
      this.empForm.travelType = travelType;
      if (this.empForm.travelType === 'Pickup') {
        this.empForm.pickupPoint = officeLocation.code;
        this.empForm.dropPoint = null;
      } else {
        this.empForm.pickupPoint = null;
        this.empForm.dropPoint = officeLocation.code;
      }

    }
  }
  validateAndSubmitForm(): void {
    if (this.employeeFormRef && this.employeeFormRef.status === 'VALID') {
      this.onSubmit();
    }
  }
  onSubmit(): void {
    this.emplyDisplayRides = this.availableRides;
    if (!this.bookNewRide) {
      if (this.availableRides) {
        const hourInMS = 60 * 60 * 1000;
        this.emplyDisplayRides = this.availableRides
          .filter(ride => {
            const userBookingTime = new Date(this.empForm.bookingTime).getTime();
            let timeMatch = false;
            if (Math.abs(userBookingTime - ride.time) < hourInMS) {
              timeMatch = true;
            }
            return ride.vehicleType === this.empForm.vehicleType &&
              ride.drop.code === this.empForm.dropPoint &&
              ride.pickup.code === this.empForm.pickupPoint && timeMatch;
          });
      }
      const ridesElem = document.getElementById('user-rides');
      if (ridesElem) {
        ridesElem.scrollIntoView({ behavior: 'smooth' });
      }
    } else {
      this.storeSvc.bookRide({
        availableSeats: this.empForm.vehicleType === 'Bike' ? 1 : 3,
        drop: this.locations.find(location => location.code === this.empForm.dropPoint),
        pickup: this.locations.find(location => location.code === this.empForm.pickupPoint),
        time: new Date(this.empForm.bookingTime),
        travellingWith: [],
        vehicleNo: this.empForm.vehicleNo,
        vehicleType: this.empForm.vehicleType,
        empId: this.empForm.empId,
      });
      this.emplyDisplayRides = null;
    }
  }
  bookRide(bookedRide: Ride): void {
    this.availableRides.forEach(ride => {
      if (ride.vehicleNo === bookedRide.vehicleNo) {
        ride.availableSeats -= 1;
      }
    });
    this.storeSvc.ridesData = [...this.availableRides];
    this.storeSvc.bookRide({
      ...bookedRide,
      empId: this.empForm.empId,
    });
    this.emplyDisplayRides = null;
    this.emplyDisplayRides = this.availableRides;
  }
}
