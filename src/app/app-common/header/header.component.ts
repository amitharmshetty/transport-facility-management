import { Component, OnInit } from '@angular/core';
import { weekDays, monthArray } from 'src/app/utils';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  getCurrentDateAndMonth(): string {
    const currentdate = new Date();
    return `${monthArray[currentdate.getMonth()]} ${weekDays[currentdate.getDay()]} ${currentdate.getFullYear()}`;
  }
}
