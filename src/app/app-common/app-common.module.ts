import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppCommonRoutingModule } from './app-common-routing.module';
import { EntryPageComponent } from './entry-page/entry-page.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [EntryPageComponent, HeaderComponent],
  imports: [
    CommonModule,
    AppCommonRoutingModule,
    FormsModule
  ],
  exports: [HeaderComponent]
})
export class AppCommonModule { }
