import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoreDataService } from 'src/app/utils';

@Component({
  selector: 'app-entry-page',
  templateUrl: './entry-page.component.html',
  styleUrls: ['./entry-page.component.scss']
})
export class EntryPageComponent implements OnInit {
  public employeeId: string;
  constructor(private router: Router) { }

  ngOnInit() {
    sessionStorage.clear();
  }

  onSubmit(): void {
    sessionStorage.setItem('auth-token', 'JWT 1234');
    sessionStorage.setItem('empId', this.employeeId);
    this.router.navigate(['transport-management']);
  }
}
