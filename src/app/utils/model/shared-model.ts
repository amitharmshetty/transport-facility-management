import { officeLocation } from '../constant';

export class EmployeeForm {
    public empId: string;
    public travelType: string;
    public pickupPoint: string;
    public dropPoint: string;
    public bookingTime: Date;
    public vehicleType: 'Car' | 'Bike';
    public vehicleNo?: string;
    constructor(data: any = {}) {
        this.bookingTime = data.bookingTime || null;
        this.empId = sessionStorage.getItem('empId') || null;
        this.travelType = data.travelType || 'Pickup';
        this.dropPoint = data.dropPoint || null;
        this.pickupPoint = data.pickupPoint || officeLocation.code;
        this.vehicleType = data.vehicleType || 'Car';
        this.vehicleNo = data.vehicleNo || undefined;
    }
}

export interface JourneyPoint {
    name: string;
    code: string;
}

export interface Ride {
    vehicleNo: string;
    vehicleType: string;
    time: any;
    pickup: JourneyPoint;
    drop: JourneyPoint;
    availableSeats: number;
    travellingWith: string[];
}



export interface UserBookedRide extends Ride {
    empId: string;
}
