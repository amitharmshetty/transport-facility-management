import { TestBed } from '@angular/core/testing';

import { ResolverDataService } from './resolver-data.service';

describe('ResolverDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResolverDataService = TestBed.get(ResolverDataService);
    expect(service).toBeTruthy();
  });
});
