import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { StoreDataService } from './store-data.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResolverDataService {

  constructor(private dataStoreSvc: StoreDataService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.dataStoreSvc.getAvailableRides()
      .pipe(
        map(data => ({ data, error: null })),
        catchError(e => of({ data: null, error: e }))
      );
  }
}
