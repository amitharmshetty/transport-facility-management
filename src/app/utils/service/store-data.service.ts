import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { Ride, JourneyPoint, UserBookedRide } from '../model';

@Injectable({
  providedIn: 'root'
})
export class StoreDataService {
  public ridesData: Ride[] | null = null;
  public locations: JourneyPoint[] = [];
  private bookedRide: BehaviorSubject<UserBookedRide> = new BehaviorSubject(null);
  constructor(private http: HttpClient) { }
  getAvailableRides(): Observable<Ride[]> {
    if (!this.ridesData) {
      return this.http.get<Ride[]>('assets/data/transport.json')
        .pipe(
          tap(data => this.ridesData = data || [])
        );
    }
    return of(this.ridesData);
  }
  populateLocations(): Observable<JourneyPoint[]> {
    if (!this.locations.length) {
      return this.http.get<JourneyPoint[]>('assets/data/locations.json')
        .pipe(
          map(data => (data || []).sort((a, b) => {
            const x = a.name.toLowerCase();
            const y = b.name.toLowerCase();
            if (x < y) {
              return -1;
            }
            if (x > y) {
              return 1;
            }
            return 0;
          })),
          tap(data => this.locations = data || [])
        );
    }
    return of(this.locations);
  }

  getBookedRide(): Observable<UserBookedRide> {
    return this.bookedRide;
  }
  bookRide(ride: UserBookedRide) {
    this.bookedRide.next(ride);
  }

}
