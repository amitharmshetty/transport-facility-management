import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    activeRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.isAllowedUser();
  }
  isAllowedUser(): boolean {
    const authToken = sessionStorage.getItem('auth-token');
    if (authToken) {
      return true;
    }
    this.router.navigate(['']);
    alert('Unauthorized Access');
    return false;
  }
}
