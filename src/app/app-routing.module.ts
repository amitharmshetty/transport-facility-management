import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './app-common/app-common.module#AppCommonModule'
  },
  {
   path: 'transport-management',
   loadChildren: './employee-transport-management/employee-transport-management.module#EmployeeTransportManagementModule'
  }
  ];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
